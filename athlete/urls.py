from django.urls import path, include
from . import views

urlpatterns = [
	path('', views.index, name='index'),
    path('record', views.record, name='record'),
	path('team', views.team, name='team'),
	path('redirect', views.redirect_test, name = 'redirect_test'),
	path('<int:record_id>/', views.detail, name='detail'),
	path('<int:record_id>/update', views.update, name='update'),
	path('<int:record_id>/delete', views.delete, name='delete'),
	path('redirect_2', views.redirect_test_2, name = 'redirect_test_2'),
	path('<int:record_team_id>/detail2', views.detail_team, name='detail_team'),
	path('<int:record_team_id>/update_team', views.update_team, name='update_team'),
	path('<int:record_team_id>/delete_team', views.delete_team, name='delete_team'),
]