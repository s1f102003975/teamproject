from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import Http404
from django.utils import timezone
from athlete.models import Record, Comment, Record_team, Comment_member

# Create your views here.
def index(request):
	return render(request, 'athlete/index.html')



def record(request):
	if request.method == 'POST':
		record = Record(
			name = request.POST['name'],
			weight = int(request.POST['weight']),
			calorie = request.POST['calorie'],
			text = request.POST['text']
		)
		record.save()
		context = {
			"record":record,
		}
		return render(request, 'athlete/detail.html', context)

	else:
		context={
			"records":Record.objects.all()
		} 
		return render(request, 'athlete/record.html', context)
	

def redirect_test(request):
	return redirect(record)

def redirect_test_2(request):
	return redirect(team)

def team(request):
	if request.method == 'POST':
		record_team = Record_team(
			team = request.POST['team'],
			date = request.POST['date'],
			text = request.POST['text']
		)
		record_team.save()
		context = {
			"record_team":record_team,
		}
		return render(request, 'athlete/detail_team.html', context)

	else:
		context={
			"record_teams":Record_team.objects.all()
		} 
		return render(request, 'athlete/team.html', context)

def detail(request, record_id):
	try:
		record = Record.objects.get(pk=record_id)
	except Record.DoesNotExist:
		raise Http404("Record does not exist")
	if request.method == 'POST':
		comment = Comment(record=record, text=request.POST['text'])
		comment.save()

	context = {
		'record':record,
		'comments': record.comments.order_by('-posted_at')
	}
	return render(request, "athlete/detail.html", context)

def detail_team(request, record_team_id):
	try:
		record_team = Record_team.objects.get(pk=record_team_id)
	except Record_team.DoesNotExist:
		raise Http404("Record does not exist")
	if request.method == 'POST':
		comment_member = Comment_member(record_team=record_team, text=request.POST['text'])
		comment_member.save()

	context = {
		'record_team':record_team,
		'comment_members': record_team.comment_members.order_by('-posted_at')
	}
	return render(request, "athlete/detail_team.html", context)

def delete_team(request, record_team_id):
	try:
		record_team_data = Record_team.objects.get(pk=record_team_id)
	except Record_team.DoesNotExist:
		raise Http404("Record does not exist")
	record_team_data.delete()
	return redirect(team)

def delete(request, record_id):
	try:
		record_data = Record.objects.get(pk=record_id)
	except Record.DoesNotExist:
		raise Http404("Record does not exist")
	record_data.delete()
	return redirect(record)

def update_team(request, record_team_id):
	try:
		record_team = Record_team.objects.get(pk=record_team_id)
	except Record_team.DoesNotExist:
		raise Http404("Record does not exist")
	if request.method == 'POST':
		record_team.name = request.POST['team']
		record_team.date = request.POST['date']
		record_team.text = request.POST['text']
		record_team.save()
		return redirect(detail_team, record_team_id)
	context = {
		"record_team":record_team
	}
	return render(request, "athlete/edit_team.html", context)

def update(request, record_id):
	try:
		record = Record.objects.get(pk=record_id)
	except Record.DoesNotExist:
		raise Http404("Record does not exist")
	if request.method == 'POST':
		record.name = request.POST['name']
		record.weight = int(request.POST['weight'])
		record.calorie = request.POST['calorie']
		record.text = request.POST['text']
		record.save()
		return redirect(detail, record_id)
	context = {
		"record":record
	}
	return render(request, "athlete/edit.html", context)