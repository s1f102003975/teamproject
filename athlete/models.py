from django.db import models

# Create your models here.

from django.utils import timezone

class Record(models.Model):
    name = models.CharField(max_length=100)
    weight = models.CharField(max_length=6)
    calorie = models.CharField(max_length=200)
    text = models.TextField()
    posted_at = models.DateTimeField(default=timezone.now)
    published_at = models.DateTimeField(blank=True, null=True)
    like = models.IntegerField(default=0)

    def published(self):
        self.published_at = timezone.now()
        
    def __str__(self):
        return self.name

class Comment(models.Model):
    text = models.TextField()
    posted_at = models.DateTimeField(default=timezone.now)
    record = models.ForeignKey(Record, related_name='comments', on_delete=models.CASCADE)

class Record_team(models.Model):
    team = models.CharField(max_length=100)
    date = models.CharField(max_length=15)
    text = models.TextField()
    posted_at = models.DateTimeField(default=timezone.now)
    published_at = models.DateTimeField(blank=True, null=True)
    like = models.IntegerField(default=0)

    def published(self):
        self.published_at = timezone.now()
        
    def __str__(self):
        return self.team

class Comment_member(models.Model):
    text = models.TextField()
    posted_at = models.DateTimeField(default=timezone.now)
    record_team = models.ForeignKey(Record_team, related_name='comment_members', on_delete=models.CASCADE)
